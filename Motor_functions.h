/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HAL_DRIVERS_MOTOR_DRIVER_MOTOR_FUNCTIONS_H_
#define HAL_DRIVERS_MOTOR_DRIVER_MOTOR_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"
#include "../../MCAL_drivers/DIO_driver/DIO_functions.h"

// DC Motor
typedef struct
{
	DIO_port_t portMotor; // Motor port
	u8 pinMotor;          // Motor pin
	u8 Motor_off_state;   // Motor off state: ON, OFF
} Motor_obj_t;

typedef const Motor_obj_t*  Motor_cptr_t;

void Motor_vidInit(Motor_cptr_t structPtrMotorCpy);

void Motor_vidActivate(Motor_cptr_t structPtrMotorCpy);
void Motor_vidDeactivate(Motor_cptr_t structPtrMotorCpy);

void Motor_vidToggle(Motor_cptr_t structPtrMotorCpy);


#endif /* HAL_DRIVERS_MOTOR_DRIVER_MOTOR_FUNCTIONS_H_ */

