/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Motor_functions.h"


inline void Motor_vidInit(Motor_cptr_t structPtrMotorCpy)
{
	DIO_vidSet_pinDirection(structPtrMotorCpy->portMotor, structPtrMotorCpy->pinMotor, OUTPUT);
	DIO_vidSet_pinValue(structPtrMotorCpy->portMotor, structPtrMotorCpy->pinMotor, structPtrMotorCpy->Motor_off_state);
}

inline void Motor_vidActivate(Motor_cptr_t structPtrMotorCpy)
{
	DIO_vidSet_pinValue(structPtrMotorCpy->portMotor, structPtrMotorCpy->pinMotor, !(structPtrMotorCpy->Motor_off_state));
}

inline void Motor_vidDeactivate(Motor_cptr_t structPtrMotorCpy)
{
	DIO_vidSet_pinValue(structPtrMotorCpy->portMotor, structPtrMotorCpy->pinMotor, structPtrMotorCpy->Motor_off_state);
}

inline void Motor_vidToggle(Motor_cptr_t structPtrMotorCpy)
{
	DIO_vidToggle_pinValue(structPtrMotorCpy->portMotor, structPtrMotorCpy->pinMotor);
}

